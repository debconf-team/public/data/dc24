###########################################################
## The DebConf sponsoring brochure in multiple languages ##
###########################################################

The source for the DebConf24 Sponsoring Brochure is found in this
directory.  It is based on the dc15 and dc17 sources.

## Dependencies

Please install dependencies before building the PDFs.  If you are using older system than Debian Stretch, you may find some packages unavailable.

  apt install texlive-xetex texlive-fonts-recommended \
  texlive-fonts-extra fonts-cantarell \
  fonts-noto-cjk fonts-noto-hinted fonts-noto-mono \
  po4a texlive-lang-korean

## The Source

The source of the formatting and the original text is the file:

	brochure.en.tex

Any change of LaTex code shall be done in that file.

## Building a single language PDF

	make brochure.XX.pdf
	# (where XX is the two-letter language code)

## Building PDFs for all languages

        make all       # (or just make)

## Cleaning

        make clean     # leaves PDFs
        make veryclean # removes all auto-generated files

## Translation infrastructure

The translation infrastructure is built around po4a [0]. This
README document aims at explaining in few steps how one can add, update
and verify translations.

[0] http://po4a.alioth.debian.org/

## Adding a new translation

To add a new translation (e.g. "french", with code "fr") for the
sponsoring brochure, one shall:

a) run make l10n/fr.po
b) start the translation using usual po-files editors
c) commit the file l10n/fr.po to Git
d) add "fr" to the $LINGUAS variable in the Makefile
e) build the brochure for a formatted review: make brochure.fr.pdf

## Update a translation

To update existing translations (e.g. "french", with code "fr") for the
sponsoring brochure, one shall:

a) update the l10n/fr.po file: make l10n/fr.po
b) update the translation using usual po-files editors
c) build the brochure: make brochure.fr.pdf
d) commit your changes

## Author and help

This (modest) README and the accompanying Makefile have been written by
Didier Raboud <odyx@debian.org> and is licenced under the WTFPL 1.1.

DC14 adjustments are by tony mancill <tmancill@debian.org> and are
licensed under CC-BY (or any other DFSG-compatible license that suits
Debian's purposes).

DC15 adjustments are also by martin f. krafft <madduck@debconf.org> and
are licenced under the WTFPL 1.1.
