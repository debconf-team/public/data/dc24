Pad for the DebCon24 Python Team BoF!

Present in the room (write your name if you feel like it):
	* Andreas Tille <tille>
	* Thomas Goirand <zigo>
	* Jonathan Carter <jcc> (highvoltage)
	* Luke Faraone <lfaraone>
	* Louis-Philippe Véronneau <pollo@d.o>
	* Faidon Liambotis <paravoid>
	* Carsten Schoenert <tijuca>
	* Utkarsh <utkarsh>
	* Stefano Rivera <stefanor@d.o>
	* Graham Inggs <ginggs>
	* Matthias Klose <doko>
	* Nilesh Patra <nilesh>
	* Edward Betts <edward>
	* Ananthu C V <weepingclown>
	* Andrey Rakhmatullin <wrar>
	* Nicolas Dandrimont <olasd>
	* Bo YU <vimer>
	* Kathara Sasikumar <kathara>
	* Shunsuke Yoshida <koedoyoshida> : 2nd BOF from PyCon JP 

Items we need to talk about:
	1. Migration to python 3.12: retrospective
	1.1 python3.12 transition was interrupted between the "add 3.12 as supported" and "default to 3.12" phases,  this resulted in many bugs being filed "early" which seemed to cause some confusion; bugs were closed without being properly fixed, and being "early" didn't even help with auto-removals, as that only comes into effect when bug severities are bumped.
	1. Default Python version for Trixie (migration to 3.13)
		1.1.  can it be done? the 3.12 transition in Debian was one of the slowest
		1.2. scan for PEP 594, removed modules, file bug reports
			1.2.1. There is already a Lintian Tag checking for PEP 594 (`uses-deprecated-python-stdlib`), available in 2.117.1
		1.3. Don't configure with any of the experimental features (no GIL)
		1.4. What's new: https://docs.python.org/3.13/whatsnew/3.13.html
	2. Address invalid escape warnings for all packages (scan build logs?) E.g. https://bugs.debian.org/1076064 (could this be something for piuparts?)
		2.1. speaking as piuparts maintainer, I at least have installation logs for all packages, so I could do a one-shot analysis. I think piuparts has infrastructure to grep logs?
		2.2. after checking, yes it can, I'll have a look when I do piuparts work this week
		2.3. piuparts wouldn't see the escape issues on test files that are not being installed
		2.4. the autopkgtest team doesn't offer a place to grep all autopkgtest logs, but if we ask them for an analysis on debian-ci@l.d.o they can try to have a look
	3. cross building third party packages, e.g. https://bugs.debian.org/1072709
		3.1. Documenting how it can be done for Extensions would be a good idea
	4. Team policy on running upstream tests during build and as autopkgtest
		4.1. SHOULD vs MUST + documenting exceptions?
		4.2. https://salsa.debian.org/python-team/tools/python-modules/-/merge_requests/24
	5. Requests for Lintian tags
		5.1. look at what's new in 3.13
	6. Decision making in team
		6.1. both the Python policy and the Debian Python Team policy have processes for updates, these processes seem to be working okay?
		6.2. For the Uploaders/Maintainer Team policy change, people who disagreed with the proposed policy update left the team before the policy change was even formally proposed?
	7. Future work:
		7.1. Migrate to pybuild-plugin-pyproject for everything?
			7.1.1. list of packages that could migrate to it but haven't yet: https://udd.debian.org/lintian/?email1=team%2Bpython%40tracker.debian.org&email2=&email3=&packages=&ignpackages=&format=html&lt_error=on&lt_warning=on&lt_information=on&lintian_tag=missing-prerequisite-for-pyproject-backend#all
			7.1.2. make it a dh_python dep
		7.2. Migrate packages to handle failing on 0 passing tests: (New feature we added in cpython 3.12, but we disabled it to stop breaking all these packages)
			7.2.1. Do it during before the 3.13 transition?
			7.2.2. pollo will open bugs if he's given a list of packages and logs :)
		7.3. Alternative python implementations
	8. Release goals for Trixie?
	9. Remote Sprint?
		9.1. People suggested "1 day everything 2 weeks" for the Python Team. eamanu tried to organise IRC meetings
			9.1.1. eamanu is interested in making this happen (Yes, I am :-) )
			9.1.2. Look at what the Perl Team did
	10. For python developers on Debian: Out of archive (or experimental) builds for all current stable cPython releases? (deadsnakes-style or something)


Notes of the 2nd Python Team BoF!

	1. Lintian / PEP 594 update
		1.1. severity: important
		1.2. https://udd.debian.org/lintian-tag.cgi?tag=uses-deprecated-python-stdlib
		1.3. list currently has a bunch of false-positive, should be fixed in the next Lintian release (code has been merged)
		1.4. code ready to do MBF
	2. I have some concerns about additional build-dependencies for running tests introducing circular dependencies.  We should encourage maintainers to use <!nocheck>
		2.1. We should add a paragraph in the current MR to change the DPT policy wrt tests
	3. "1 day each 2 weeks" sprint update
		3.1. People don't have to commit a whole day
		3.2. Use a wiki page to coordinate common goals and QA work
		3.3. an email will be sent to the list
	4. Removal of intersphinx external objects.inv access (see also https://bugs.debian.org/1032593
		4.1. A pybuild rewrite would probably be useful to implement sphinx builds in dh_python
	5. PyCon Announcement https://pycon.asia/ .
		5.1. JP : https://2024.pycon.jp/
		5.2. APAC: https://2024-apac.pycon.id/
	6. Provide current best-practice documentation on the wiki (ready)
		6.1. There is https://wiki.debian.org/Python#Encouraged_practices but it's out of date
	7. DPT admins
	8. 3.13 transition
		8.1. package all the PEP-594 alternatives listed
			8.1.1. pollo will make a list of what needs to be packaged and what's already been
		8.2. package some new packages implementing support for removed "dead batteries"?
			* https://github.com/simonrob/pyasyncore
			* https://github.com/tiran/legacycrypt
			0.0.1. https://peps.python.org/pep-0594/
			0.0.2. eamanu will have a look at what could be done for this
