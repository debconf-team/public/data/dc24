# Ideas for the DebConf25

All the proposals must use Debian somehow.

### Proposal 01: Sensors, arduino and interactive music

Make the music creation interactive and collaborative by using cheap sensor packs such as proximity, light, 6dof etc. These get translated to midi notes that then arrange a prepared orchestration in a variety of melodic and aesthetically pleasing ways.

The user will have the feeling of agency of creating music and the music will always be pleasant. We will follow this up perhaps a day later with a talk on how we executed it for a very affordable price.

Proposal 01 author: Chris McKenzie <chris@9ol.es>

### Proposal 02: DebConf Algorave, community live coding performances

Invite people to play live coding during the DebConf, it can be during the cheese and wine, or we can schedule a specific night and a specific place for it. Maybe in a collaboration with other local communities, such as: Paris Cookie Collective.

You can see a complete proposal at:
https://wiki.debian.org/Teams/LiveCoding/DebConfAlgorave/ProposalDebConf23Algorave

Proposal 02 author: Joenio













### Proposal 03: DebConf Art Space, hacklab for art experimentation + 1 night event for live performances

A hacklab art space dedicated for art experimentation during the DebCamp and DebConf, and one night event for live presentations to share the artwork created during the DebCamp+DebConf days in the hacklab art.

- Video art
- Video mapping
- Live coding
- Netart, gif
- Performancee and choreography
- Sound art
- Interactive art
- Augmented reality
- Experimental music
- Poetry
- Analog synths, digital synths, softsynths
- DJ playing music using DJ tools packaged on Debian
- Etc....

If you like the idea please say hello at: joenio@debian.org

Proposal 03 author: Joenio and Mari

(ADD YOUR PROPOSAL HERE)



