= Meeting notes =

Participants:
	* DLange
	* meskes
	* Jonathan Carter
	* Edward Betts
	* Daniel Lenharo
	* Thomas Lange
	* Ilu
	* zigo
	* Nicolas Dandrimont (olasd)
	* Noah Meyerhans
	* Santiago Ruano
	* Pete Ryland
	* Andreas Tille
	* Carsten Shoenert (tijuca)
	* noel
	* Giyeon Bang
	* Seonghee Jin
	* Teodor (Teo) Bobirnila
	* Yunseong Kim
	* pollo
	* zumbi
	* tumbleweed
	* enkelenaH
	* Mathias Gibbens
	* Kurt
	* Changwoo Ryu
	* phls


Todos:
	* Ilu / Zumbi: Gather group to draft TO association contracts
	* highvoltage: Aron Williamson contact to Andreas incl. briefing for the DPL

Agreements:
	* We want to do contracts with our TOs
	* Partners team should be delegated (overlaps with DebConf fundraising but does not fully replace it)
		* People that want to join, send an email to Andreas (lange, highvoltage, DLange will)
	* Debian should professionalize more (what does this even mean?)
	* A Debian foundation is an option to be more professional
	* "really move forward" (c) Illu

= Pre-meeting notes from highvoltage =

I'm adding some information for those not familiar with our issues, which might be useful to study up before the meeting. These notes are meant to provide a very brief summary of our situation and thoughts of the future, do not take it as any form of gospel.

Feel free to make corrections or add comments where needed. --highvoltage

== Current Debian structure ==

Debian is an association of volunteers, governed by the Debian constitution.. A volunteer association is a legal entity, although not an incorporated one. Some countries  (and some states in the US) do not recognise volunteer associations, and they don't have any legal status at all, while in other parts of the world, they can do everything from open bank accounts, enter contracts, buy insurance and seperate (at least some) liability from project members.

In Debian, we're an association that makes use of Fiscal Sponsors. A Fiscal Sponsor is an incorporated organisation (usually a non-profit) that aids with the financial/administration aspects for an independent project or organisation. In Debian, we call our Fiscal Sponsors "Trusted Organisations" or TOs for short.

Originally, Software in the Public Interest (SPI) was founded to act as Debian's Fiscal Sponsor. It's been a success story of it's own, and SPI now acts as a fiscal sponsor for many free software projects. SPI is incorporated as a 501(c)(3) non-profit in the US. We also have two TOs in Europe: Debian France and Debian.ch. Both of those are volunteer associations. - Debian France is an "association loi 1901", so it has a legal status/persona that goes further than an unincorporated "volunteer association". It also has fiscal non-profit status that applies to donations made in France. I believe Debian.ch has a similar status under Swiss law

In my opinion, our structure has actually held up remarkable well over the last 30 years, although as we grow and continue to evolve (along with the world around us), there has been some major cracks and problems that are becoming increasingly urgent to solve, more on that below.

A few links:
More about volunteer associations: https://en.wikipedia.org/wiki/Voluntary_association
Debian Constitution: https://www.debian.org/devel/constitution
More about fiscal sponsors: https://en.wikipedia.org/wiki/Fiscal_sponsorship
More about SPI: https://www.spi-inc.org/p

== Problems ==

* Currently, Debian has no formal relationships / contracts with our TOs, this has already been a problem in a case where a TO has become defunct. It also means that we have no leverage on any aspect because we can't make any claim to service
* TOs have their own policies in place, which are mostly good, especially in the case of Debian France and Debian.ch, but any TO can change its policies at any time without any say from Debian
* Our TO that we rely on most is extremely slow on paperwork / contracts, which has hurt us in significant ways over the years. Also, conflicting "gentlemen agreements" have caused great unhappiness, especially in the debconf-sponsors team.
* Over my DPL terms, I've blocked new TOs until we have the structure in place to set up agreements with TOs, whichever that structure might be. Having a TO in the BRICS area would be beneficial, and we currently have at least the Brazillian team requesting TO status for their local fiscal sponsor, but imho we should sort out the Debian side first before adding more TOs
* With our current structure, Debian doesn't provide much limited liability, and may expose our members (and especially the DPL) to legal trouble on behalf of the project

A few links:

- https://wiki.debian.org/Teams/DPL/TrustedOrganizationCriteria

== Potential solutions, from the perspective of highvoltage ==

I believe that it's clear that Debian needs to incorporate an entity that represents the goals of its members and users, what is less clear is exactly the form it has to take.

The founding of a foundation isn't particularly hard, but maintaining it is a lot of work. Most free software projects who go the foundation route, flounder on their first audit.

The benefits of a foundation is that we could receive tax-deductable donations, but on the other hand, we already have that via SPI.

I believe it's better for Debian to register a non-profit project in a sensible country that represents Debian itself, while keeping the TO model. This way we get the benefits of each TO (easier to pay local providers and volunteers, lower costs to work with local currencies, ideally faster turnaround time for payments, etc).

How do we go about this? It's probably a good idea to reach out to one or more consultants who can advise on this, and then once a few good options can be formulated, take it to the project for a vote. This will no doubt need some changes in our constitution, so it would ultimately formally have to rely on a vote.
