The debian.social team BoF Agenda

	1. Welcome
	2. State of services
		2.1. Pleroma: pleroma.debian.social
			2.1.1. Is it still the state of the art? Maybe it's time we evaluate all the options
			2.1.2. 2 promising looking forks. Rhonda intends to look into them within the next few weeks
			2.1.3. Should we consider Mastodon again? This would lose data when migrating. You can migrate the account and follower lists. If we hosted a mastodon it should probably be in parallel.
		2.2. Pixelfed: pixelfed.debian.social
			2.2.1. IIRC an upgrade required upgrading some base infra. Rhonda can have a look at upgrading
			2.2.2. Can we promote using pixelfed for DebConf.
		2.3. Peertube: peertube.debian.social
			2.3.1. Not in a fantastic state. We're stuck on 5.x. We'll wait for a new server for the upgrade.
			2.3.2. We've been forgetting to update the home-page to link to recent events.
			2.3.3. Live streaming?
				2.3.3.1. Depends on the new server migration (needs an IP)
		2.4. WriteFreely: writefreely.debian.social
			2.4.4. Remove?
			2.4.5. I've been discouraging people from signing up to this recently unless they actually want this --highvoltage
			2.4.6. Not enough data to be worth doing an organized static export. Contact the posters to backup their content, and shut it down.
		2.5. WordPress: wordpress.debian.social
			2.5.1. Mild use.
		2.6. Matrix: matrix.debian.social/element.debian.social
			2.6.1. DB is very disk hungry
			2.6.2. there are alternatives to appservice irc bridging - heisenbridge? #libera-matrix on libera could be a good contact point. Paddatrapper to investigate.
			2.6.3. Request for Telegram bridging. olasd could put us in touch with some Telegram-bridge admins
		2.7. Plume
			2.7.1. Dormant upstream for 2 years.
			2.7.2. Only 2 posts on our instance.
			2.7.3. This should probably be shut down.
		2.8. Jitsi: jitsi.debian.social
			2.8.1. Very popular, but stores no local data or has much interesting about it, probably better suited to have on debian.net?
	3. Future plans
		3.9. New big server
		3.10. https://storm.debian.net/shared/h8aoupp4hppmJWeKbybf8R6_J1AIj2XupA0fY1hz_X-
		3.11. some new services we'd want on https://wiki.debian.org/Teams/DebianSocial
	4. Insert your items here
	5. Current spend:
		5.1. Debian.social clouds - €131.80 + standalone server: €73.05
		5.2. 
