DC24 newcomer survivor guide

Main debconf mailing lists:
    https://lists.debian.org/debconf-announce/
    https://lists.debian.org/debconf-discuss/
What is IRC?
    https://wiki.debian.org/IRC
How to connect to keep IRC logs while offline?
    https://wiki.debian.org/IRC/ElementMatrix
    https://element.debian.social/

DC24 Daytrip registration
    https://debconf24.debconf.org/daytrip/


