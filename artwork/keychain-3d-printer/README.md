# 3D Printing Parameters for the DebConf 24 Keychains

### Filament:
- Sunlu Black Meta PLA: https://a.co/d/497RDnb
- Sunlu White Meta PLA: https://a.co/d/foUazGv
- Sunlu Blue PLA+: https://a.co/d/5PxSqAg
- eSun Red PLA+: https://a.co/d/1H0hwjp\
*OR*
- Sunlu Red PLA+ (Only available bundled with Orange): https://a.co/d/fLKqrEX 


### Printer Hardware
- 0.2mm nozzle is best for ensuring the text doesn't get squished

### Slicer Parameters (in mm)
##### (use .3mf file to import these automatically)
- Layer height: 0.1
- First layer height: 0.15
- Line width: 0.22
- Resolution: 0.012
- X-Y hole compensation: 0.1
- X-Y contour compensation 0.15
- Top and Bottom infill patterns: Monotonic
- Infill Density: 60% with Gyroid pattern

>For multicolor print with single extruder:\
Given the same layer height, you should get about 57 layers.
The order of the colours is: ***Black, White, Black, Red, Blue***\
Color change gcode pauses should be at layers: ***15, 29, 45, 49***

