List of used fonts:

  - URW Bookman [`fonts-urw-base35`](https://packages.debian.org/sid/fonts-urw-base35)
  - Pretendard [`fonts-pretendard`](https://packages.debian.org/sid/fonts-pretendard)